"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.compose = (...functions) => {
    return (x) => {
        return functions.reduce((input, f) => {
            return f(input);
        }, x);
    };
};
var Promises;
(function (Promises) {
    Promises.bind = (f) => {
        return (p) => {
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                try {
                    // what if f returns a promise?
                    let result = f(yield p); // <-- if p is rejected, catch block will be triggered
                    if (result instanceof Promise) {
                        resolve(yield result);
                    }
                    else {
                        resolve(result);
                    }
                }
                catch (error) {
                    reject(error);
                }
            }));
        };
    };
})(Promises = exports.Promises || (exports.Promises = {}));
