export declare const compose: (...functions: ((x: any) => any)[]) => (x: any) => any;
export declare namespace Promises {
    const bind: (f: (x: any) => any) => (x: any) => any;
}
