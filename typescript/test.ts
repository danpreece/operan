import * as Operators from "./index"

let add1 = (x: number) => { return x + 1 }
let multiplyBy3 = (x: number) => { return x * 3 }
let logIt = (x: number) => { 
	console.log(x) 
	return true
}

let add1AndMultiplyBy3 = Operators.compose(add1, multiplyBy3)
console.log("composed function should echo 9")
Operators.compose(add1AndMultiplyBy3, logIt)(2)

let add1AndMultiplyBy3AndAdd1Again = Operators.compose(add1AndMultiplyBy3, add1)
console.log("composed function should echo 10")
Operators.compose(add1AndMultiplyBy3AndAdd1Again, logIt)(2)

// sequences
let sequenece = Operators.compose(add1, multiplyBy3, logIt)
console.log("composed function should echo 9")
sequenece(2)

let returnsPromise = (x: number): Promise<number> => {
	return new Promise((resolve, reject) => {
		resolve(x)
	})
}

// should error
let shouldError = Operators.compose(returnsPromise, add1)
console.log("if this worked it would echo 5")
Operators.compose(shouldError, logIt)(4)

// first need to bind add1 to a promise
let shouldWork = Operators.compose(returnsPromise, Operators.Promises.bind(add1))
console.log("this should work and should echo 7")
Operators.compose(shouldWork, Operators.Promises.bind(logIt))(6)