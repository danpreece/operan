type OneParameterFunction = (x: any) => any

export const compose = (...functions: OneParameterFunction[]): OneParameterFunction => {
	return (x: any) => {
		return functions.reduce((input: any, f: OneParameterFunction) => {
			return f(input)
		}, x)
	}
}

export namespace Promises {
	export const bind = (f: OneParameterFunction): OneParameterFunction => {
		return (p: Promise<any>): Promise<any> => {
			return new Promise(async (resolve, reject) => {
				try {
					// what if f returns a promise?
					let result = f(await p) // <-- if p is rejected, catch block will be triggered
					if (result instanceof Promise) {
						resolve(await result)
					} else {
						resolve(result)
					}
					
				} catch (error) {
					reject(error)
				}
			})
		}
	}
}
